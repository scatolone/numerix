import { NgModule } from '@angular/core';
import { IntegerDirective } from './integer.directive';
import { FloatDirective } from './float.directive';
import { NaturalDirective } from './natural.directive';



@NgModule({
  declarations: [IntegerDirective, FloatDirective, NaturalDirective],
  imports: [
  ],
  exports: []
})
export class NumerixModule { }
