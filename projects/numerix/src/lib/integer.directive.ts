import { Directive, ElementRef } from '@angular/core';
import { Numeric } from './numeric';

@Directive({
  selector: '[integer]'
})
export class IntegerDirective extends Numeric {

  validRegex = /^(0|[1-9][0-9]*|-[1-9][0-9]*)$/;

  buildingRegex = /^(0|-?([1-9][0-9]*)?)$/;
  
  constructor(_el: ElementRef) {
    super(_el)
  }
}
