import { ElementRef, EventEmitter, Input, Output, HostListener } from '@angular/core';

export abstract class Numeric {
    
    validRegex : RegExp;

    buildingRegex : RegExp;

    constructor(private _el: ElementRef) { }
  
    @Input() alwaysNegative: boolean = false;
    @Output() invalid: EventEmitter<any> = new EventEmitter();
    @Output() valid: EventEmitter<any> = new EventEmitter();
  
    private specialKeys = ['Control', 'Alt', 'Backspace', 'Backspace', 'Tab', 'End', 'Home', 'ArrowLeft', 'ArrowRight', 'Delete'];
  
    @HostListener('keydown', ['$event']) onKeydown(event) {
  
      let isCopying = event.ctrlKey && event.key == 'c';
      let isPasting = event.ctrlKey && event.key == 'v';
      let isCutting = event.ctrlKey && event.key == 'x';
      let isSelectingAll = event.ctrlKey && event.key == 'a';
      let cpca = isCopying || isPasting || isCutting || isSelectingAll;

      const value = this._el.nativeElement.value;
      const key = event.key;
      const caretStart = this._el.nativeElement.selectionStart;
      const caretEnd = this._el.nativeElement.selectionEnd;
      const resultValue = value.slice(0,caretStart) + key + value.slice(caretEnd);

      if (resultValue.match(this.buildingRegex) == null && !this.specialKeys.includes(event.key) && !cpca) {
        event.preventDefault();
        this.invalid.emit();
      } else {
        this.valid.emit();
      }
  
    }
  
    @HostListener('paste', ['$event']) onPaste(event) {
      const value = this._el.nativeElement.value;
      const clipboard = event.clipboardData.getData('text/plain');

      const caretStart = this._el.nativeElement.selectionStart;
      const caretEnd = this._el.nativeElement.selectionEnd;
      const resultValue = value.slice(0,caretStart) + clipboard + value.slice(caretEnd);
      
      if (!resultValue.match(this.buildingRegex)) {
        event.preventDefault();
        this.invalid.emit();
      } else {
        this.valid.emit();
      }
    }

    @HostListener('change', ['$event']) onChange(event) {
      const initialValue = this._el.nativeElement.value;
      if (!initialValue.match(this.validRegex)) {
        this._el.nativeElement.value = "";
        this.invalid.emit();
      } else {
        this.valid.emit();
      }
    }
}
