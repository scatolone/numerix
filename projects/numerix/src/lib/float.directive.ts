import { Directive, ElementRef, Input } from '@angular/core';
import { Numeric } from './numeric';

@Directive({
  selector: '[float]'
})
export class FloatDirective extends Numeric {

  @Input() separator;
  @Input() maxDecimals;

  DEFAULT_SEPARATOR = '\\.';

  validRegex;

  buildingRegex;


  constructor(_el: ElementRef) {
    super(_el);
    this.buildRegex();
  }

  escapeRegExp(stringToGoIntoTheRegex) {
    return stringToGoIntoTheRegex.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
  }

  ngOnChanges() {
    this.buildRegex();
  }

  buildRegex(){
    let s = this.separator ? this.escapeRegExp(this.separator) : this.DEFAULT_SEPARATOR;
    let d1 = "+";
    let d2 = "*";
    if(this.maxDecimals){
      try{
        if(parseFloat(this.maxDecimals)<=0){
          throw new Error();        }
        d1 = `{1,${this.maxDecimals}}`;
        d2 = `{0,${this.maxDecimals}}`;
      }catch(e){
        console.log("maxDecimals should be a valid number > 0");
      }
    }
    this.validRegex = new RegExp(`^(0|-?0${s}\\d${d1}|-?[1-9][0-9]*(${s}\\d${d1})?)$`);
    this.buildingRegex = new RegExp(`^(0|-?(0(${s}\\d${d2})?)?|-?([1-9][0-9]*(${s}\\d${d2})?)?)$`);
  }
}
