import { Directive, ElementRef } from '@angular/core';
import { Numeric } from './numeric';

@Directive({
  selector: '[natural]'
})
export class NaturalDirective extends Numeric{

  validRegex = /^(0|[1-9]+)$/;

  buildingRegex = /^(0|[1-9]+)$/;

  constructor(_el: ElementRef) {
    super(_el)
  }

}
