# numerix

*numerix* is an Angular 2 library (tested on version 8) which offers a bunch of directives for handling numbers inside input text fields. Basically it validates the input according to the directive used and it doesn't allow the user to type invalid characters.

### Installing

```
npm install -s numerix
```

### Usage
The library provides directives for handling natural, integer and floating point numbers. You can use the directives just by importing them into your *app.module.ts* file like follows:

```typescript
//app.module.ts

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IntegerDirective, FloatDirective, NaturalDirective } from 'numerix';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent,
    IntegerDirective,
    NaturalDirective,
    FloatDirective
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
```
After the directives have been imported, you can use them with input text fields. The following example show all the directives and all the possible options for each directive.

### Example

```html

<table>
  <tbody>
    <tr><td>Natural:    </td><td><input (valid)="valid()" (invalid)="invalid()" natural></td></tr>
    <tr><td>Integer:    </td><td><input (valid)="valid()" (invalid)="invalid()" integer></td></tr>
    <tr><td>Float(,):   </td><td><input (valid)="valid()" (invalid)="invalid()" separator="," float></td></tr>
    <tr><td>Float(.):   </td><td><input (valid)="valid()" (invalid)="invalid()" float></td></tr>
    <tr><td>Float(.2):  </td><td><input (valid)="valid()" (invalid)="invalid()" maxDecimals="2" float></td></tr>
    <tr><td>Float(,1):  </td><td><input (valid)="valid()" (invalid)="invalid()" separator="," maxDecimals="1" float></td></tr>
  </tbody>
</table>
```
*valid* and *invalid* events are fired whenever respectively valid and invalid text (according to the directive used) is typed into the text field. 



## Authors

* **Maurizio Macrì**
## License

This project is licensed under the ISC License
